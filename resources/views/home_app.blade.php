<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <link href="<?=asset('js/jquery-ui/jquery-ui.min.css'); ?>" media="all" rel="stylesheet">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
            .work_space{
                position: absolute;
                top:100px;
                bottom: 100px;
                left:300px;
                right:100px;
                border: 5px solid #0000C2;
                overflow: hidden;
            }
            .board_selector{
                position: absolute;
                bottom: 50px;
                left: 300px;
            }
            .block{
                position: absolute;
                overflow: hidden;
                border: 3px solid #880000;
                background-color: #2ca02c;
                z-index: 0;
            }
            .block .inside_img{
                max-width: 100%;
                max-height: 100%;;
            }
            .block.active{
                border: 3px solid #b70000;
                background-color: #37ca37;
                z-index: 10;
            }
            .block.deleted{
                display: none;
            }
            .control_panel{
                position: absolute;
                top:50px;
                left:300px;
            }
            .img_conteiner{
                position: absolute;
                top:50px;
                left: 50px;
                margin-bottom: 50px;
                width: 200px;
                border: 5px solid #0000C2;
            }
            .img_conteiner img{
                position: relative;
                margin-top: 30px;
                width: 100px;
                left: 10px
                right: 10px;
                z-index: 20;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="img_conteiner">
                    @foreach ($files as $file)
                        <img src="/img/{{$file}}">
                    @endforeach
                </div>
                <div class="control_panel">
                    <input id="board_name" placeholder="Board Name">
                    <button id="save_button">Save board</button>
                    <button id="new_box_button">New box</button>
                    <button id="delete_box_button">Delete box</button>
                </div>
                <select class="board_selector"></select>
                <div class="work_space">

                </div>
            </div>
        </div>
        <script src="<?=asset('js/jquery-2.1.4.min.js'); ?>" language="JavaScript"></script>
        <script src="<?=asset('js/jquery-ui/jquery-ui.min.js'); ?>" language="JavaScript"></script>
        <script src="<?=asset('js/main.js'); ?>" language="JavaScript"></script>
    </body>
</html>
