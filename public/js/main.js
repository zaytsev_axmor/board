/**
 * Created by Home on 07.07.2015.
 */
$(document).ready(function () {
    var update_board = function (ad_id) {
        $.getJSON('/ad', function (request) {
            $('.board_selector').empty().append('<option value="0">New</option>')
            for (k in request) {
                var checked = ad_id == request[k]._id;
                $('.board_selector').append('<option value="' + request[k]._id + '"'+
                (checked?' selected ':'')+'>' + request[k].name + '</option>');
            }
        });
    }
    update_board(0);
    $('.board_selector').change(function () {
        $('#board_name').val($('.board_selector option:checked').text());
        $.getJSON('/ad/' + $(this).val() + '/block', function (request) {
            var board = $('.work_space').empty();
            for (k in request) {
                board.append('<div class="block" id="' + request[k]._id + '" style="left:' + request[k].offset_x +
                'px; top:' + request[k].offset_y + 'px; width:' + request[k].width + 'px; height:' + request[k].height +
                'px"></div>');
                if (typeof (request[k].img_url)!='undefined'){
                    $('.work_space .block:last').append('<img class="inside_img" src="'+request[k].img_url+'" />');
                }
            }
            $('.work_space .block').draggable({
                start: function () {
                    $('.work_space .block.active').removeClass('active');
                    $(this).addClass('active');
                }
            }).droppable({
                hoverClass: "hover",
                accept: '.img_conteiner img',
                tolerance: 'pointer',
                drop: function( event, ui ) {
                    $(this).find('.inside_img').remove()
                    $(this).append('<img class="inside_img" src="'+ui.draggable[0].currentSrc+'" />');
                }
            }).resizable({
                start: function () {
                    $('.work_space .block.active').removeClass('active');
                    $(this).addClass('active');
                }
            }).click(function () {
                $('.work_space .block.active').removeClass('active');
                $(this).addClass('active');
            });
        });
    });
    $('#new_box_button').click(function () {
        $('.work_space').append('<div class="block" style="left:10px; top:10px; width:100px; height:100px"></div>');
        $('.work_space .block').draggable({
            start: function () {
                $('.work_space .block.active').removeClass('active');
                $(this).addClass('active');
            }
        }).droppable({
            hoverClass: "hover",
            accept: '.img_conteiner img',
            tolerance: 'pointer',
            drop: function( event, ui ) {
                $(this).find('.inside_img').remove()
                $(this).append('<img class="inside_img" src="'+ui.draggable[0].currentSrc+'" />');
            }
        }).resizable({
            start: function () {
                $('.work_space .block.active').removeClass('active');
                $(this).addClass('active');
            }
        }).click(function () {
            $('.work_space .block.active').removeClass('active');
            $(this).addClass('active');
        });
    });
    $('#delete_box_button').click(function () {
        $('.work_space .block.active').removeClass('active').addClass('deleted');
    });
    $('#save_button').click(function () {
        $.get('/csrf', function (token) {
            var save_block = function (obj, ad_id) {
                var offset = obj.position();
                var save_data = {
                    _token: token,
                    offset_x: offset.left,
                    offset_y: offset.top,
                    width: obj.width(),
                    height: obj.height(),
                    ad_key: ad_id
                }
                if (obj.find('.inside_img').length > 0){
                    save_data.img_url = obj.find('.inside_img').attr('src');
                }
                if (obj.attr('id')) {
                    $.ajax({
                        url: '/block/' + obj.attr('id'),
                        method: (obj.is('.deleted') ? 'DELETE' : 'PUT'),
                        data: save_data,
                        dataType: 'json',
                        success: function () {
                            if (obj.is('.deleted')){
                                obj.remove();
                            }
                        }
                    });
                } else {
                    $.ajax({
                        url: '/block',
                        method: 'POST',
                        data: save_data,
                        dataType: 'json',
                        success: function (result) {
                            alert(result.new_id);
                            obj.attr('id', result.new_id);
                        }
                    });
                }
            }
            var ad_id = $('.board_selector').val();
            $.ajax({
                url: '/ad' + (ad_id!='0' ? '/' + ad_id : ''),
                method: (ad_id!='0' ? 'PUT' : 'POST'),
                data: {
                    name: $('#board_name').val(),
                    _token: token
                },
                dataType: 'json',
                success: function (result) {
                    if (ad_id=='0'){
                        ad_id = result.new_id;
                    }
                    update_board(ad_id);
                    $('.work_space .block').each(function(){
                        save_block($(this),ad_id);
                    })
                }
            });
        });
    });

    $( ".img_conteiner img" ).draggable({ helper: "clone",  scroll: false, refreshPositions: true });
});