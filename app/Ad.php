<?php

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Ad extends Eloquent
{
    protected $collection = 'ad';
}
