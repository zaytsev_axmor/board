<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Block;

use Validator;

class BlockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $blocks = Block::all();
        return json_encode($blocks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset_x' => 'required|integer',
            'offset_y' => 'required|integer',
            'width' => 'required|integer',
            'height' => 'required|integer',
            'ad_key' => 'required|exists:ad,_id',
            'img_url' => '',
        ]);
        if ($validator->fails()) {
            return json_encode(['errors'=>$validator->errors()->all()]);
        }
        $block = new Block();
        $block->offset_x = (int) $request->input('offset_x');
        $block->offset_y = (int) $request->input('offset_y');
        $block->width = (int) $request->input('width');
        $block->height = (int) $request->input('height');
        $block->ad_key = $request->input('ad_key');
        if (!empty($request->input('img_url'))){
            $block->img_url = $request->input('img_url');
        }
        return json_encode(['result' => (bool)$block->save(), 'new_id'=>$block->_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $block = Block::find($id);
        return json_encode($block);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'offset_x' => 'required|integer',
            'offset_y' => 'required|integer',
            'width' => 'required|integer',
            'height' => 'required|integer',
            'ad_key' => 'required|exists:ad,_id',
            'img_url' => '',
        ]);
        if ($validator->fails()) {
            return json_encode(['errors'=>$validator->errors()->all()]);
        }
        $block = Block::find($id);
        if (!empty($block)){
            $block->offset_x = (int) $request->input('offset_x');
            $block->offset_y = (int) $request->input('offset_y');
            $block->width = (int) $request->input('width');
            $block->height = (int) $request->input('height');
            $block->ad_key = $request->input('ad_key');
            if (!empty($request->input('img_url'))){
                $block->img_url = $request->input('img_url');
            }
            return json_encode(['result' => (bool)$block->save()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return json_encode(['result' => (bool)Block::destroy($id)]);
    }
}
