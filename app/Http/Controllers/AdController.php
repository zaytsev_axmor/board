<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ad;
use App\Block;
use Validator;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $ads = Ad::all();
        return json_encode($ads);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|between:3,50|unique:ad,name',
        ]);
        if ($validator->fails()) {
            return json_encode(['errors'=>$validator->errors()->all()]);
        }
        $ad = new Ad();
        $ad->name = $request->input('name');
        return json_encode(['result' => (bool)$ad->save(),'new_id'=>$ad->_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $ad = Ad::find($id);
        return json_encode($ad);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|between:3,50|unique:ad,name',
        ]);
        if ($validator->fails()) {
            return json_encode(['errors'=>$validator->errors()->all()]);
        }
        $ad = Ad::find($id);
        if (!empty($ad)) {
            $ad->name = $request->input('name');
            return json_encode(['result' => (bool)$ad->save()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Ad::destroy($id)){
            $blocks = Block::where('ad_key', '=', $id);
            return json_encode(['result' => (bool)$blocks->delete()]);
        }
        return false;
    }
}
