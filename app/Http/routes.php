<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    $files = scandir('./img/');
    unset($files[array_search('.', $files)]);
    unset($files[array_search('..', $files)]);
    return view('home_app', ['files' => $files]);
});
Route::get('/csrf', 'CSRFController@index');

Route::resource('ad', 'AdController', ['except' => ['create', 'edit']]);

Route::resource('block', 'BlockController', ['except' => ['create', 'edit']]);

Route::resource('ad.block', 'AdBlockController', ['only' => ['index']]);