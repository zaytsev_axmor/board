<?php

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Block extends Eloquent
{
    protected $collection = 'block';
}
